package com.xfdmao.fcat.coin.service;

import com.xfdmao.fcat.coin.entity.BtcHour;
import com.xfdmao.fcat.common.service.BaseService;

import java.util.List;

/**
 * Created by fier on 2018/09/20
 */
public interface BtcHourService extends BaseService<BtcHour>{
    Boolean batchInsert(List<BtcHour> btcHourList);

    List<BtcHour> getByPeriod(String period);
}

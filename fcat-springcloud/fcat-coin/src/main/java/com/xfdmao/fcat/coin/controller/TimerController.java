package com.xfdmao.fcat.coin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * Created by fier on 2018/10/22
 */
@RestController
public class TimerController {

    @Autowired
    private BtcHourController btcHourController;

    @Autowired
    private HttpController httpController;
    @Scheduled(cron = "0 0/5 * * * ?")
/*    public void getBitinfocharts(){
        new Thread(() -> httpController.btcByBitinfocharts()).start();
    }*/
    //@Scheduled(cron = "0 5 8 * * ?")
    public void getTime1(){
        new Thread(() -> btcHourController.getBtcHour("btcusdt","1week","2")).start();
        new Thread(() -> btcHourController.getBtcHour("btcusdt","1mon","2")).start();
        new Thread(() -> btcHourController.getBtcHour("btcusdt","1year","2")).start();
        new Thread(() -> btcHourController.getBtcHour("btcusdt","1day","2")).start();
        new Thread(() -> btcHourController.getBtcHour("btcusdt","60min","26")).start();
        new Thread(() -> btcHourController.getBtcHour("btcusdt","5min","300")).start();
        new Thread(() -> btcHourController.getBtcHour("btcusdt","4hour","7")).start();
    }


    //@Scheduled(cron = "0 0/5 * * * ?")
    public void getTime2(){
        new Thread(() -> btcHourController.getBtcHour("btcusdt","5min","2")).start();
    }

    //@Scheduled(cron = "0 0/60 * * * ?")
    public void getTime3(){
        new Thread(() -> btcHourController.getBtcHour("btcusdt","60min","2")).start();
        new Thread(() -> btcHourController.getBtcHour("btcusdt","4hour","2")).start();
    }
}
